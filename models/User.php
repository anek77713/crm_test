<?php

namespace app\models;

use dektrium\user\models\User as BaseUser;
use yii\helpers\ArrayHelper;

class User extends BaseUser
{

    public static function getUserDataList() { // could be a static func as well
        $models = self::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'username');
    }

}
