<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171116_093315_profile_first_last_name
 */
class m171116_093315_profile_first_last_name extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'first_name', Schema::TYPE_STRING."(255) DEFAULT NULL AFTER `user_id`");
        $this->addColumn('{{%profile}}', 'last_name', Schema::TYPE_STRING."(255) DEFAULT NULL AFTER `first_name`");
        $this->addColumn('{{%profile}}', 'patronymic', Schema::TYPE_STRING."(255) DEFAULT NULL AFTER `last_name`");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171116_093315_profile_first_last_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171116_093315_profile_first_last_name cannot be reverted.\n";

        return false;
    }
    */
}
