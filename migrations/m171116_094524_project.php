<?php

use dektrium\user\migrations\Migration;

/**
 * Class m171116_094524_project
 */
class m171116_094524_project extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%project}}', [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer(11)->notNull(),
            'name'       => $this->string(255),
            'start_at'   => 'datetime DEFAULT NULL',
            'end_at'     => 'datetime DEFAULT NULL',
            'created_at' => 'datetime DEFAULT NULL',
            'updated_at' => 'datetime DEFAULT NULL',
            'created_by' => 'int(11) DEFAULT NULL',
            'updated_by' => 'int(11) DEFAULT NULL'
        ], $tableOptions);

        $this->addForeignKey('{{%fk_user_project}}', '{{%project}}', 'user_id', '{{%user}}', 'id', $this->cascade, $this->restrict);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171116_094524_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171116_094524_project cannot be reverted.\n";

        return false;
    }
    */
}
