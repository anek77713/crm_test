<?php

use yii\db\Migration;

/**
 * Class m171116_093524_admin_user
 */
class m171116_093524_admin_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
         $this->insert('user',array(
                 'email'         =>'admin@admin.admin',
                 'username'      =>'admin',
                 'password_hash' => \Yii::$app->security->generatePasswordHash('admin', 10),
                 'confirmed_at'  => time(),
                 'created_at'    => time(),
                 'updated_at'    => time()
          ));

         $this->insert('profile',array(
                'user_id' => \Yii::$app->db->getLastInsertId(),
                'first_name' => 'Admin',
                'last_name' => 'Admin',
                'patronymic' => 'Admin'
          ));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171116_093524_admin_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171116_093524_admin_user cannot be reverted.\n";

        return false;
    }
    */
}
